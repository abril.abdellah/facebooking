import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.facebook.com/')

WebUI.click(findTestObject('Object Repository/Page_Facebook - Connexion ou inscription/a_Crer un compte'))

WebUI.setText(findTestObject('Object Repository/Page_Facebook - Connexion ou inscription/input_Impossible de crer votre compteImposs_6f0b92'), 
    'zefzf')

WebUI.setText(findTestObject('Object Repository/Page_Facebook - Connexion ou inscription/input_Impossible de crer votre compteImposs_e3ea0e'), 
    'zefzf')

WebUI.setText(findTestObject('Object Repository/Page_Facebook - Connexion ou inscription/input_Impossible de crer votre compteImposs_cf9066'), 
    'zfzef')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Facebook - Connexion ou inscription/input_Impossible de crer votre compteImposs_0e7c18'), 
    'grv05yYFyaM=')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Facebook - Connexion ou inscription/select_janfvmaravrmaijunjuilaosepoctnovdc'), 
    '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Facebook - Connexion ou inscription/select_202020192018201720162015201420132012_85bfe4'), 
    '1999', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Facebook - Connexion ou inscription/select_123456789101112131415161718192021222_566da4'), 
    '19', true)

WebUI.click(findTestObject('Object Repository/Page_Facebook - Connexion ou inscription/label_Homme'))

WebUI.click(findTestObject('Object Repository/Page_Facebook - Connexion ou inscription/div_Une erreur sest produite. Veuillez ress_982fdf'))

WebUI.click(findTestObject('Object Repository/Page_Facebook - Connexion ou inscription/button_Sinscrire'))

WebUI.closeBrowser()

